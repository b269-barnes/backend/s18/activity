// console.log("Hello World!");

// [SECTION] Parameters and Arguments
function printInput(){
    let nickname = prompt("Enter your nickname: ");
    console.log("Hi, " + nickname);
}
printInput();

// However, for some use cases, this may not be ideal.
// We can use parameter and argument to pass our inputs.

// Consider this function:

// (name) -> parameter
function printName(name){
    console.log("My name is " + name);
}

// ("Juana") -> argument to be passed on the function parameter
printName("Juana");
printName("Romenick");
printName("Jane");

let variableName = "Alexander";

printName(variableName);

// Create a function which can be re-used to check for a number's divisibility instead of manually doing it.

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisivleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisivleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments
// Function parameters can also accept function arguments
// Some complex functions use other functions as arguments to perform mor complicated results.
// This will be further seen when we discuss array methods.

function argumentFunction(name){
    console.log("Hello " + name);
    console.log("this function was passed as an argument.");
}

argumentFunction();

function invokeFunction(argumentFunction){
    argumentFunction();
}

// Adding and removing the () impacts the output of JS heavily
// When a function is used with (), It denotes invoking/calling
// A function used without () is normally associated with using function as an argument.

invokeFunction(argumentFunction);

// Multiple parameters and arguments

function createFullName(firstName, middleName, lastName){
    console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");
createFullName("Romenick", "Barredo", "Garcia");

// providing more or less arguments than the expected parameters
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "Hello");

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

function printFullName(middleName, firstName, lastName){
    console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan", "Dela", "Cruz");

// The return statement
// the return statement allows us to output a value from a function to be passed to the line/block.

function returnFullName (firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName

// return will not allow any other line of codes to be executed below it.
	console.log("Hello");
	console.log("Hello");
	console.log("Hello");
	console.log("Hello");
	console.log("Hello");

}

// This will not display out because it is not the proper way to call a function with return.
returnFullName("Jeffrey", "Smith", "Bezos");

// Correct way of call a function with return statement
// Whatever value is returned from the returnFullname() is stored in the completeName variable.

let completeName = returnFullName("Jeffrey", "Smith", "Bezos")

console.log(completeName); 

console.log(returnFullName("Jeffrey", "Smith", "Bezos"))
// You can also create a variable inside the function to contain the result and return that variable instead.

function returnAddress(city, country){
	let fullAddres = city + ", " + country;
	return fullAddres
}

let myAddress = returnAddress("Cebu City","Philippines");

console.log(myAddress);

// Doing the same function calling the return using console.log

function printPlayerInfo(username, level, job){

	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);

}

let user1 = printPlayerInfo("knight_white", 95, "Paladin")
console.log(user1);

// You cannot save any value from printPlayerInfo() because it does not return anything.